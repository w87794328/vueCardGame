import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    hero: {
      health: 0,//生命值
      handCard: [],//手牌
      cardGroup: ['card1', 'card2', 'card1', 'card2', 'card1', 'card2', 'card1', 'card2'],
      fightingCardGroup: [],//战斗牌组
      cardGraveyard: [],//墓地
    },
    monster: {
      health: 0,//生命值
      handCard: [],//手牌
      cardGroup: ['card1', 'card1', 'card1', 'card1', 'card1', 'card2', 'card1', 'card2'],
      fightingCardGroup: [],//战斗牌组
      cardGraveyard: [],//墓地
    },
    TabComponent: 'Hello'
  },
  mutations: {
    //加载怪物数据
    loadMonster (state, monster) {
      state.monster.health = monster.health
      state.monster.handCard = []
      state.monster.fightingCardGroup = state.monster.cardGroup
    },
    //加载英雄数据
    loadHero (state, hero) {
      state.hero.health = hero.health
      state.hero.handCard = []
      state.hero.fightingCardGroup = state.hero.cardGroup
    },
    //攻击
    hit (state, event) {
      state[event.target].health -= event.value
    },
    //摸牌
    getHandCard (state, value) {
      for (let i = value.num; i > 0; i--) {
        console.log(i+'fightingCardGroup:'+state[value.target].fightingCardGroup)
        if(state[value.target].fightingCardGroup.length < 1){
          state[value.target].fightingCardGroup = state[value.target].cardGraveyard
          state[value.target].cardGraveyard = []
        }
        // if(state[value.target].fightingCardGroup.length < 1){
        //   console.log('牌用完了')
        //   break
        // }
        state[value.target].handCard.push(state[value.target].fightingCardGroup[0])
        state[value.target].fightingCardGroup.splice(0,1)
      }
    },
    //把使用过的手牌加入墓地
    removeHandCard (state, value) {
      state[value.target].cardGraveyard.push(state[value.target].handCard[value.index])
      state[value.target].handCard.splice(value.index, 1)
    },
    //受伤
    injure (state) {
      state.hero.health -= this.state.monster.attack
    },
    //路由
    router (state, data) {
      state.TabComponent = data
    }
  }
})